function NeedsMoreSpace {
    param (
        [int]$MinSpaceGB = 8
    )
    $NeedsCleanup = $false
    $ListDisk = Get-WmiObject -Class Win32_LogicalDisk
    foreach ($Disk in $ListDisk) {
        if ($Disk.size -ne $NULL) {
            if (($Disk.freespace / 1GB) -lt $MinSpaceGB) {
                $NeedsCleanup = $true
            }
        }
    }
    return $NeedsCleanup
}

function EnsureMSYS2 {
    If(!(Test-Path "C:/msys64")) {
        [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12

        $env:MSYSTEM = 'MSYS'
        $env:CHERE_INVOKING = 'yes'

        Start-BitsTransfer -Source 'https://github.com/msys2/msys2-installer/releases/download/nightly-x86_64/msys2-base-x86_64-latest.sfx.exe' -Destination 'msys2.exe'
        .\msys2.exe -y -oC:\
        Remove-Item .\msys2.exe
        git -C C:/msys64 init
        C:/msys64/usr/bin/bash -lc ' '
        C:/msys64/usr/bin/bash -lc "pacman --noconfirm -Syyuu --overwrite '**'"
        C:/msys64/usr/bin/bash -lc "pacman --noconfirm -Syyuu --overwrite '**'"
        C:/msys64/usr/bin/bash -lc "bash -x ./ci/setup_msys2.sh"
        git -C C:/msys64 config user.email "foo@bar"
        git -C C:/msys64 config user.name "Foo Bar"
        git -C C:/msys64 add .
        git -C C:/msys64 commit --quiet -m "first-commit"
    }
}

function KillAll {
    taskkill /f /fi "USERNAME eq ${env:username}" /fi "IMAGENAME ne explorer.exe" /fi "IMAGENAME ne gitlab-runner.exe" /fi "PID ne $PID" /fi "IMAGENAME ne conhost.exe" | Out-Null
    Get-WmiObject -Class Win32_Process -Filter "Name != 'explorer.exe' AND Name != 'gitlab-runner.exe' AND ProcessId != $PID AND Name != 'conhost.exe'" | Remove-WMIObject -ErrorAction SilentlyContinue
    C:/msys64/usr/bin/bash -lc "ps | awk '!(NR<=1) {print `$1}' | xargs kill -9"
}

function ResetState {
    If(Test-Path "$env:USERPROFILE\.local") {
        Remove-Item -Recurse -Force "$env:USERPROFILE\.local"
    }
    If(Test-Path "C:/msys64/.git/index.lock") {
        Remove-Item C:/msys64/.git/index.lock
    }
    git -C C:/msys64 clean -qxfdf --exclude='*.pkg.tar.*'
    git -C C:/msys64 reset -q HEAD --hard
}

function EnsureSpace {
    if (NeedsMoreSpace -MinSpaceGB 8) {
        Remove-Item -Recurse -Force "C:\_r\_builds"
        Remove-Item -Recurse -Force "C:\_r\_cache"
        git -C C:/msys64 clean -qxfdf
    }
}

# On the first run, set everything up
EnsureMSYS2

# Try to kill everything not currently running
KillAll

# Reset the filesystem to some degree
ResetState

# Clean things up if there is not enough space
EnsureSpace
