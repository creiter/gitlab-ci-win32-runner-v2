# Windows Runner Setup

Hetzner Cloud Specific:

* Using a Hetzner Cloud CX21 instance, ~6€ a month
* Using Windows Server 2016
* Follow https://wiki.hetzner.de/index.php/Windows_on_Cloud/en

Base Install:

* Install Windows
* Select a good Administrator password (it gets used for remote login)
* Activate Windows
* Prepare Ansible:
  `Invoke-Expression ((New-Object System.Net.Webclient).DownloadString('https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1'))`

Ansible:

* Run `cp ansible/hosts.example ansible/hosts` and add login password, gitlab tokens
* Install everything: `ansible-playbook -i hosts setup.yml`
* Install the runner: `ansible-playbook -i hosts setup_runner.yml`
* Windows Updates/Clean things: `ansible-playbook -i hosts update.yml`
