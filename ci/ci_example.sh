#!/bin/bash

set -e

# Update everything
pacman --noconfirm -Suy --overwrite '**'

# Install the required packages
pacman --noconfirm -S --needed \
    ${MINGW_PACKAGE_PREFIX}-cc \
    ${MINGW_PACKAGE_PREFIX}-meson \
    ${MINGW_PACKAGE_PREFIX}-ninja

cd ci/project
meson setup _build
